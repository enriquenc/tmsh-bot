from enum import Enum

class DeviceTypes(Enum):
	SocketDevice = 1
	Door = 2
	TemperatureSensors = 3
