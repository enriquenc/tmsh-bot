from device import Device
from device_types import DeviceTypes

class SocketDevice(Device):
	def __init__(self, id):
		self.type = DeviceTypes.SocketDevice
		self.id = id