import telebot
from telebot.apihelper import send_message
import mqtt_subscribe
import subprocess
import json
from tg_image_db import *
import requests

#[TODO] Make Database
users = dict()

admin_id = 345727276

try:
	f = open("token", 'r')
	bot = telebot.TeleBot(f.readline())
	f.close()
except:
	print("Invalid token.")
	exit(0)

@bot.message_handler(content_types=['photo'])
def photo(message):
	print(message.photo[1].file_id)

@bot.message_handler(commands=['start'])
def start(message):
	if message.chat.id == admin_id:
		print("admin")
	bot.send_message(message.chat.id, "Welcome to TM SMART HOME! ✨\n\n")

@bot.message_handler(commands=['t'])
def temperature(message):
	data = mqtt_subscribe.get_data()
	if data != []:
		data = json.loads(data.decode('utf-8'))
		text_message = "*Data from Xiaomi Bluetooth Thermometer*\n\n"
		text_message += "🪟 room: Bedroom\n"
		text_message += "🌡 temperature: " + str(data["temperature"]) + "\n"
		text_message += "💧 humidity: " + str(data["humidity"]) + "\n\n"
		text_message += "🔋 battery: " + str(data["battery"]) + "\n" 
		bot.send_photo(message.chat.id, mi_temp_image, text_message, parse_mode='Markdown')
	else:
		bot.send_message(message.chat.id, "no data yet")


def makeKeyboard():
	markup = telebot.types.InlineKeyboardMarkup()
	markup.add(telebot.types.InlineKeyboardButton(text="ON", callback_data="ON"),
			telebot.types.InlineKeyboardButton(text="OFF", callback_data="OFF"))
	return markup

relay_state = "OFF 📴"

def generate_switch_message():
	global relay_state
	text_message = "*Data from Sonoff Smart Relay*\n\n"
	text_message += "🪟 room: Bedroom\n"
	text_message += "🖱 relay is " + relay_state
	return text_message

@bot.message_handler(commands=['s'])
def switch(message):
	bot.send_photo(message.chat.id, sonoff_image)
	bot.send_message(message.chat.id, generate_switch_message(), parse_mode='Markdown', reply_markup=makeKeyboard())


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
	global relay_state
	if call.data in relay_state:
		return
	
	r = requests.get("http://192.168.1.7/?m=1&o=1")

	if "ON" in r.text:
		relay_state = "ON ✅"
	else:
		relay_state = "OFF 📴"
		
	bot.edit_message_text(chat_id=call.message.chat.id,
						message_id=call.message.message_id,
						text=generate_switch_message(),
						reply_markup=makeKeyboard(),
						parse_mode='Markdown')

if __name__ == "__main__":
	bot.polling()


