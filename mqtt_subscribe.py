import paho.mqtt.client as mqtt

mqtt_username = "maslo"
mqtt_password = "admin"
mqtt_topic = "temperature"
mqtt_broker_ip = "192.168.1.12"

client = mqtt.Client()
client.username_pw_set(mqtt_username, mqtt_password)

data = b'{"temperature": 25.4, "humidity": 45, "voltage": 2.806, "calibratedHumidity": 45, "battery": 71, "timestamp": 1621771805, "sensor": "a4:c1:38:80:57:61", "rssi": 0, "receiver": "raspberrypi"}'

def get_data():
    return data


# These functions handle what happens when the MQTT client connects
# to the broker, and what happens then the topic receives a message
def on_connect(client, userdata, flags, rc):
    # rc is the error code returned when connecting to the broker
    print("Connected!", str(rc))
    
    # Once the client has connected to the broker, subscribe to the topic
    client.subscribe(mqtt_topic)
    
def on_message(client, userdata, msg):
    # This function is called everytime the topic is published to.
    # If you want to check each message, and do something depending on
    # the content, the code to do this should be run in this function
    global data
    data = msg.payload
    print("Topic: ", msg.topic + "\nMessage: " + str(msg.payload))
    
    # The message itself is stored in the msg variable
    # and details about who sent it are stored in userdata

# Here, we are telling the client which functions are to be run
# on connecting, and on receiving a message
client.on_connect = on_connect
client.on_message = on_message



# Once everything has been set up, we can (finally) connect to the broker
# 1883 is the listener port that the MQTT broker is using
client.connect(mqtt_broker_ip, 1883)
client.loop_start()
# Once we have told the client to connect, let the client object run itself
#client.loop_forever()
# client.disconnect()
