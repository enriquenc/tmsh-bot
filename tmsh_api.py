from device_types import DeviceTypes
from enum import Enum
from socket_device import SocketDevice

def api_func(device_type, id, action, param=None):
	# url = "some_url_for_device_request"
	# call apropriate function
	return 0

def api_get_all_devices():
	return [SocketDevice(1)]


class SocketActions(Enum):
	Off = 0
	On = 1
	GetData = 2

class TMSHapi_Socket:
	@staticmethod
	def on(id):
		api_func(DeviceTypes.SocketDevice, id, SocketActions.Off)
		return 1

	@staticmethod
	def off(id):
		api_func(DeviceTypes.SocketDevice, id, SocketActions.Off)
		return 1

	@staticmethod
	def get_data():
		api_func(DeviceTypes.SocketDevice, id, SocketActions.GetData)
		return "Socket data"

class TMSHapi_Door:
	@staticmethod
	def get_data():
		api_func(DeviceTypes.Door, id, SocketActions.Door)
		return "Door data"

class TMSHapi:
	@staticmethod
	def get_all_socket_data():
		devices = api_get_all_devices()
		data = []
		for d in devices:
			if d.type == DeviceTypes.SocketDevice:
				data.append(TMSHapi_Socket.get_data(d.id))
		return data
